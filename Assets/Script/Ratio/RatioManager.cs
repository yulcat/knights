﻿using UnityEngine;
using System.Collections;

public class RatioManager : MonoBehaviour {
	private float height;
	private float width;
	private float heightPerWidth;
	private Camera cam;
	private float originalSize;

	/*
	 *   This component makes Letter box
	 *   put this component in Camera object
	 */

	void Awake(){
		if(GetComponent<Camera>()==null)
			return;
		
		cam = GetComponent<Camera>();
		originalSize = cam.orthographicSize;
	}
	
	void OnEnable () {
		height = Screen.height;
		width = Screen.width;
		heightPerWidth = height/width;
	//	Debug.Log(heightPerWidth);
		cam.orthographicSize = heightPerWidth * originalSize / (9f/16f);
	}
}
