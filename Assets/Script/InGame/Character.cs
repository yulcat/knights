﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using TeamsAndCharacters;
using CommonConstants;

public class Character : InteractableObject
{
	public int Attack;
	public int Move;

	public int CharacterIndex;

	public int[] IndexHP;
	public int[] IndexAttack;
	public int[] IndexMove;

	public List<Vector2> AttackableTilesForAcht;
	public List<Vector2> AttackableTilesForBlitz;
	public List<Vector2> AttackableTilesForJulius;
	public List<Vector2> AttackableTilesForMika;
	public List<Vector2> AttackableTilesForNaura;
	public List<Vector2> AttackableTilesForPy;
	public List<Vector2> AttackableTilesForSera;
	public List<Vector2> AttackableTilesForSesto;
	public List<Vector2> AttackableTilesForRuby;
	public List<Vector2> AttackableTilesForZen;
	public List<Vector2> AttackableTilesForAiry;
	public List<Vector2> AttackableTilesForAnna;
	public List<Vector2> AttackableTilesForArkadi;
	public List<Vector2> AttackableTilesForCaml;
	public List<Vector2> AttackableTilesForMichelle;
	public List<Vector2> AttackableTilesForRanard;
	public List<Vector2> AttackableTilesForRoshanak;
	public List<Vector2> AttackableTilesForSchnell;
	public List<Vector2> AttackableTilesForSuyeon;
	public List<Vector2> AttackableTilesForTigres;

	public List<List<Vector2>> AttackableTileLists = new List<List<Vector2>>();

	public bool Activated;
	public bool Moving;

	public GameManager Manager;

	public int ObjectBuffDuration;

	public Directions Direction;

	public int GameCharacterIndex;

	public GameManager.TurnPhases TurnPhase;

	public GameObject[] AttackEffects;

	public AudioClip[] AttackSounds;
	public AudioClip HitSound;
	public AudioSource SoundEffect;

	public Tile startTile;
	public Directions startDirection;
	public float startScaleX;

	public List<InteractableObject> HitObjects;

	public bool isAlreadyAttacked;

	public GameObject Effect;

	public SpriteRenderer ShowIfMyTeam;

	void Awake()
	{
		SoundEffect = GetComponent<AudioSource>();
	}

	void Start()
	{
		MaxHP = Constants.TickHP * IndexHP [CharacterIndex] + Constants.StandardHP;

		SetCurrentHP ();

		Attack = Constants.TickAttack * IndexAttack [CharacterIndex] + Constants.StandardAttack;
		Move = IndexMove [CharacterIndex];

		AttackableTileLists.Add (AttackableTilesForAcht);
		AttackableTileLists.Add (AttackableTilesForBlitz);
		AttackableTileLists.Add (AttackableTilesForJulius);
		AttackableTileLists.Add (AttackableTilesForMika);
		AttackableTileLists.Add (AttackableTilesForNaura);
		AttackableTileLists.Add (AttackableTilesForPy);
		AttackableTileLists.Add (AttackableTilesForRuby);
		AttackableTileLists.Add (AttackableTilesForSera);
		AttackableTileLists.Add (AttackableTilesForSesto);
		AttackableTileLists.Add (AttackableTilesForZen);
		AttackableTileLists.Add (AttackableTilesForAiry);
		AttackableTileLists.Add (AttackableTilesForAnna);
		AttackableTileLists.Add (AttackableTilesForArkadi);
		AttackableTileLists.Add (AttackableTilesForCaml);
		AttackableTileLists.Add (AttackableTilesForMichelle);
		AttackableTileLists.Add (AttackableTilesForRanard);
		AttackableTileLists.Add (AttackableTilesForRoshanak);
		AttackableTileLists.Add (AttackableTilesForSchnell);
		AttackableTileLists.Add (AttackableTilesForSuyeon);
		AttackableTileLists.Add (AttackableTilesForTigres);

		if(!GameManager.isMyCharacter(this))
		{
			ShowIfMyTeam.enabled = false;
		}
	}

	void OnMouseDown()
	{
		if(Activated && GameManager.Turn && GameManager.isMyCharacter(this))
		{
			if(TurnPhase == GameManager.TurnPhases.Wait && GameManager.MovingCharacter == null)
			{
				TurnPhase = GameManager.TurnPhases.Move;
				GameManager.GlobalTurnPhase = GameManager.TurnPhases.Move;
				Moving = true;

				GameManager.MovingCharacter = this;

				GetComponent<Animator>().SetTrigger("Select");

				ShowMovableTilesOfMovingCharacter();

				GameManager.DeactivateCharacterColliders();
			}
		}
	}

	void ShowMovableTilesOfMovingCharacter()
	{
		GameManager.DeactivateAllTiles();

		List<Tile> MovableTiles = GameManager.MovingCharacter.MovableRange();

		foreach(Tile MovableTile in MovableTiles)
		{
			if(MovableTile != null)
			{
				MovableTile.MyRenderer.sprite = MovableTile.MovableSprite;
				MovableTile.Movable = true;
			}
		}
	}

	void OnMouseEnter()
	{
		if(TurnPhase == GameManager.TurnPhases.Wait)
		{
			List<Tile> TileList = ShowAndReturn_AttackRange();
			foreach(Tile MovableTile in MovableRange())
			{
				if(MovableTile != null)
				{
					if(MovableTile.MyRenderer.sprite == MovableTile.AttackableSprite)
					{
						MovableTile.MyRenderer.sprite = MovableTile.MovableAndAttackableSprite;
					}
					else
					{
						MovableTile.MyRenderer.sprite = MovableTile.MovableSprite;
					}
				}
			}
		}
	}

	void OnMouseExit()
	{
		if(TurnPhase == GameManager.TurnPhases.Wait)
		{
			GameManager.DeactivateAllTiles();

			if(GameManager.GlobalTurnPhase == GameManager.TurnPhases.Move)
			{
				ShowMovableTilesOfMovingCharacter();
			}
		}
	}

	List<Tile> MovableRange()
	{
		List<Tile> MoveRange = new List<Tile> ();

		for(int i = 1; i <= Move; i++)
		{
			foreach(Vector2 MovableTile in Manager.MoveLevels[i-1])
			{
				MoveRange.Add(GameManager.GetTileAt(CurrentTile.X+(int)(MovableTile.x), CurrentTile.Y+(int)(MovableTile.y)));
			}
		}

		return MoveRange;
	}

	void Update()
	{
		if(TurnPhase == GameManager.TurnPhases.Direction && GameManager.GlobalTurnPhase == GameManager.TurnPhases.Direction)
		{
			Vector3 RelativeMousePosition = Camera.main.ScreenToWorldPoint(Input.mousePosition) - transform.position;
			if(RelativeMousePosition.y > -RelativeMousePosition.x && RelativeMousePosition.y < RelativeMousePosition.x)
			{
				Direction = Directions.Right;
			}
			else if(RelativeMousePosition.y > -RelativeMousePosition.x && RelativeMousePosition.y > RelativeMousePosition.x)
			{
				Direction = Directions.Up;
			}
			else if(RelativeMousePosition.y < -RelativeMousePosition.x && RelativeMousePosition.y > RelativeMousePosition.x)
			{
				Direction = Directions.Left;
			}
			else if(RelativeMousePosition.y < -RelativeMousePosition.x && RelativeMousePosition.y < RelativeMousePosition.x)
			{
				Direction = Directions.Down;
			}
			SetCharacterSpriteDirection(Direction);

			List<Tile> AtackRange = ShowAndReturn_AttackRange();

			if(Input.GetMouseButtonDown(0))
			{
				FixDirection();
			}

			if(Input.GetMouseButton(1))
			{
				transform.position = startTile.transform.position + relativePosition_AboutTile(startTile.Y);
				CurrentTile.CharacterOnTile = null;
				CurrentTile = startTile;
				Direction = startDirection;
				Vector3 Scale = transform.localScale;
				Scale.x = startScaleX;
				transform.localScale = Scale;
				DamageText.gameObject.transform.localScale = Scale;

				CurrentTile.CharacterOnTile = this;

				TurnPhase = GameManager.TurnPhases.Move;
				GameManager.GlobalTurnPhase = GameManager.TurnPhases.Move;
				GameManager.DeactivateCharacterColliders();

				Moving = true;

				GameManager.MovingCharacter = this;

				GameManager.DeactivateAllTiles();

				List<Tile> MovableTiles = MovableRange();

				foreach(Tile MovableTile in MovableTiles)
				{
					if(MovableTile != null)
					{
						MovableTile.MyRenderer.sprite = MovableTile.MovableSprite;
						MovableTile.Movable = true;
					}
				}
			}
		}
	}

	void SetCharacterSpriteDirection(Directions dir)
	{
		Vector3 Scale = transform.localScale;

		if(dir == Directions.Right)
		{
			Scale.x = -1;
		}
		else if(dir == Directions.Left)
		{
			Scale.x = 1;
		}
		transform.localScale = Scale;
		DamageText.gameObject.transform.localScale = Scale;
	}

	public void FixDirection()
	{
		int[] MoveInfo = new int[4];

		MoveInfo [0] = GameCharacterIndex;
		MoveInfo [1] = CurrentTile.X;
		MoveInfo [2] = CurrentTile.Y;
		MoveInfo [3] = Manager.DirectionToInteger (Direction);

		List<Tile> AttackRange = Manager.ShowAndReturn_AttackRange (this);

		TurnPhase = GameManager.TurnPhases.Wait;
		GameManager.GlobalTurnPhase = GameManager.TurnPhases.Wait;
		GameManager.RegisterHitObject(AttackRange, GameManager.MovingCharacter);
		GameManager.DeactivateAllTiles();
		Activated = false;
		Moving = false;
		GameManager.MovingCharacter = null;

		NetworkManager.SendMoveInformation (MoveInfo);
	}

	Tile RotatedRelativeTile(int X, int Y)
	{
		if(Direction == Directions.Right)
		{
			return GameManager.GetTileAt(CurrentTile.X+X, CurrentTile.Y+Y);
		}
		else if(Direction == Directions.Up)
		{
			return GameManager.GetTileAt(CurrentTile.X-Y, CurrentTile.Y+X);
		}
		else if(Direction == Directions.Left)
		{
			return GameManager.GetTileAt(CurrentTile.X-X, CurrentTile.Y-Y);
		}
		else
		{
			return GameManager.GetTileAt(CurrentTile.X+Y, CurrentTile.Y-X);
		}
	}

	Vector2 RotatedVector2(int X, int Y)
	{
		if(Direction == Directions.Right)
		{
			return new Vector2((float)(CurrentTile.X+X), (float)(CurrentTile.Y+Y));
		}
		else if(Direction == Directions.Up)
		{
			return new Vector2((float)(CurrentTile.X-Y), (float)(CurrentTile.Y+X));
		}
		else if(Direction == Directions.Left)
		{
			return new Vector2((float)(CurrentTile.X-X), (float)(CurrentTile.Y-Y));
		}
		else
		{
			return new Vector2((float)(CurrentTile.X+Y), (float)(CurrentTile.Y-X));
		}
	}

	public List<Tile> ShowAndReturn_AttackRange()
	{
		GameManager.DeactivateAllTiles();
		List<Tile> AttackRange = new List<Tile>();

		foreach(Vector2 AttackableTile in AttackableTiles())
		{
			AttackRange.Add (RotatedRelativeTile((int)(AttackableTile.x), (int)(AttackableTile.y)));
		}

		GameManager.ChangeSpriteOfTiles(AttackRange, CurrentTile.AttackableSprite);

		return AttackRange;
	}

	public List<Vector2> AttackableTiles()
	{
		return AttackableTileLists [CharacterIndex];
	}

	public void InstantiateEffect()
	{
		GameObject EffectPrefab = AttackEffects[CharacterIndex];

		if(CharacterIndex == 1)
		{
			Effect = Instantiate(EffectPrefab, GameManager.GetTilePositionAt(RotatedVector2(6, 0))-new Vector3(0, 0, 3), Quaternion.identity) as GameObject;
		}
		else if(CharacterIndex == 2)
		{
			Effect = Instantiate(EffectPrefab, (GameManager.GetTilePositionAt(RotatedVector2(3, 0))+GameManager.GetTilePositionAt(RotatedVector2(3, -1)))/2-new Vector3(0, 0, 3), Quaternion.identity) as GameObject;
		}
		else if(CharacterIndex == 6)
		{
			Effect = Instantiate(EffectPrefab, (GameManager.GetTilePositionAt(RotatedVector2(3, 0))+GameManager.GetTilePositionAt(RotatedVector2(4, 0)))/2-new Vector3(0, 0, 3), Quaternion.identity) as GameObject;
		}
		else
		{
			Effect = Instantiate(EffectPrefab, transform.position-new Vector3(0, 0, 2), Quaternion.identity) as GameObject;

			if(CharacterIndex == 0 || CharacterIndex == 5 || CharacterIndex == 7 || CharacterIndex >= 10)
			{
				Vector3 Rotation = Effect.transform.localRotation.eulerAngles;

				Debug.Log (Manager.Names[CharacterIndex]+" Attacks "+Direction.ToString());
				if(Direction == Directions.Right)
				{
					Rotation.z = 0;
				}
				else if(Direction == Directions.Up)
				{
					Rotation.z = 90;
				}
				else if(Direction == Directions.Left)
				{
					Rotation.z = 180;
				}
				else
				{
					Rotation.z = 270;
				}

				Effect.transform.localRotation = Quaternion.Euler(Rotation);
			}
		}
	}

	public static Vector3 relativePosition_AboutTile(int tileY)
	{
		return new Vector3 (0, 0, tileY * Constants.diffPositionZ - 2);
	}

	public IEnumerator DoAttack(int index)
	{
		Debug.Log ("DoAttack of Attacker "+index+"is Activated.");
		GetComponent<Animator> ().SetTrigger ("Attack");
		InstantiateEffect ();

		SoundEffect.PlayOneShot(AttackSounds[CharacterIndex], 1.0f);

		yield return new WaitForSeconds (Constants.timeToHitAni);

		foreach(InteractableObject HitObject in HitObjects)
		{
			int chainCount = HitObject.CharactersForAttack.Count;

			if(HitObject.Buff)
			{
				HitObject.totalDamage += 1;
				HitObject.DamageText.text = HitObject.totalDamage+"";
				GameManager.PutHeal(this, (int) (MaxHP*chainMultiplierForHeal(chainCount)/10));
			}
			else
			{
				HitObject.totalDamage += (int) (Attack*chainMultiplierForAttack(chainCount));
				HitObject.DamageText.text = chainCount+" Chain\n"+HitObject.totalDamage;
				if(ObjectBuffDuration > 0)
				{
					GameManager.PutHeal(this, (int) (Attack*chainMultiplierForAttack(chainCount)*0.3f));
				}
				HitObject.GetComponent<Animator>().SetTrigger("Hit");
				SoundEffect.PlayOneShot(HitSound, 1.0F);
			}
		}

		yield return new WaitForSeconds (1-Constants.timeToHitAni);

		Debug.Log ("GameManager's AttackersCount is " + GameManager.Attackers.Count);
		Debug.Log ("Attacker index is " + index);

		Destroy (Effect.gameObject);

		if(GameManager.Attackers.Count > index+1)
		{
			StartCoroutine(GameManager.Attackers[index+1].DoAttack(index+1));
		}
		else
		{
			SoundEffect.PlayOneShot(HitSound, 1.0F);
			StartCoroutine(GameManager.ApplyDamageForAll());
		}

		CutScene CurrentCutScene = GameObject.FindObjectOfType (typeof(CutScene)) as CutScene;
		if(CurrentCutScene != null)
		{
			Destroy(CurrentCutScene.gameObject);
		}
	}

	public float chainMultiplierForAttack(int chainCount)
	{
		if(chainCount == 1)
		{
			return 1.0f;
		}
		else if(chainCount == 2)
		{
			return 1.25f;
		}
		else if(chainCount == 3 || chainCount == 4)
		{
			return 1.5f;
		}
		else
		{
			return 0.0f;
		}
	}

	public float chainMultiplierForHeal(int chainCount)
	{
		if(chainCount == 1)
		{
			return 1.0f;
		}
		else if(chainCount == 2)
		{
			return 1.25f;
		}
		else if(chainCount == 3 || chainCount == 4)
		{
			return chainCount/2;
		}
		else
		{
			return 0;
		}
	}
}