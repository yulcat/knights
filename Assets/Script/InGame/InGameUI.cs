﻿using UnityEngine;
using System.Collections;
using TeamsAndCharacters;
using CommonConstants;

public class InGameUI : MonoBehaviour
{
	public bool Player;

	public GameObject[] HpBars;

	public SpriteRenderer ShowTeam;
	public Sprite Pintos;
	public Sprite Haskell;

	public Sprite[] Portraits;

	public SpriteRenderer[] PortraitRenderers;

	public SpriteRenderer SkillBarYellow;
	public SpriteRenderer SkillBarRed;

	public const float ScaleOfHpBar = 4.5f;

	void Start()
	{
		if(Network.isServer == Player)
		{
			if(GameManager.myTeam == Teams.Pintos)
			{
				ShowTeam.sprite = Pintos;
			}
			else
			{
				ShowTeam.sprite = Haskell;
			}
		}
		else
		{
			if(GameManager.otherTeam == Teams.Pintos)
			{
				ShowTeam.sprite = Pintos;
			}
			else
			{
				ShowTeam.sprite = Haskell;
			}
		}
	}

	public void SetCharacterNames ()
	{
		for (int i = 0; i < Constants.ofTeamMembers; i++)
		{
			Debug.Log (ReturnCPT_IfNotPlayer (Player) + i + "");

			if (Portraits [GameManager.Characters [ReturnCPT_IfNotPlayer (Player) + i].CharacterIndex] != null) 
			{
				PortraitRenderers [i].sprite = Portraits [GameManager.Characters [ReturnCPT_IfNotPlayer (Player) + i].CharacterIndex];
			}
		}
	}

	void Update()
	{
		int UserSkillCharge;
		if(Player == Network.isServer)
		{
			UserSkillCharge = GameManager.myUserSkillCharge;
		}
		else
		{
			UserSkillCharge = GameManager.enemyUserSkillCharge;
		}

		if(UserSkillCharge <= 100)
		{
			Vector3 YellowScale = SkillBarYellow.transform.localScale;
			YellowScale.x = UserSkillCharge*7/100;
			SkillBarYellow.transform.localScale = YellowScale;

			Vector3 RedScale = SkillBarRed.transform.localScale;
			RedScale.x = 0;
			SkillBarRed.transform.localScale = RedScale;
		}
		else
		{
			Vector3 YellowScale = SkillBarYellow.transform.localScale;
			YellowScale.x = 7;
			SkillBarYellow.transform.localScale = YellowScale;

			Vector3 RedScale = SkillBarRed.transform.localScale;
			RedScale.x = (UserSkillCharge-100)*7/100;
			SkillBarRed.transform.localScale = RedScale;
		}

		for(int i = 0; i < Constants.ofTeamMembers; i++)
		{
			Character Knight = GameManager.Characters[ReturnCPT_IfNotPlayer(Player)+i];
			Vector3 Scale = HpBars[i].transform.localScale;

			if(Knight == null)
			{
				Scale.x = 0;
			}
			else if(Knight.MaxHP != 0)
			{
				Scale.x = HpRatio(Knight) * ScaleOfHpBar;
			}

			HpBars[i].transform.localScale = Scale;
		}
	}

	int ReturnCPT_IfNotPlayer(bool Player)
	{
		if(Player == true)
		{
			return 0;
		}
		else
		{
			return Constants.ofTeamMembers;
		}
	}

	float HpRatio(Character Knight)
	{
		return (float)(Knight.HP) / (float)(Knight.MaxHP);
	}
}