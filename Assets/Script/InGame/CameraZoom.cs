﻿using UnityEngine;
using System.Collections;

public class CameraZoom : MonoBehaviour {
	
	private float zoom;
	public float zoommin;
	public float zoommax;
	public float zoomSensitivity;
	public float dragSpeed = 1;
	private Vector3 dragOrigin;
	private float minXmove;
	private float maxXmove;
	private float minYmove;
	private float maxYmove;
	
	// Use this for initialization
	void Start () {
		zoom=27;
	}
	
	// Update is called once per frame
	void Update () {
		minXmove=(float)(zoom*0.9417+0.875);
		maxXmove=(float)(51.5-zoom*0.9333);
		minYmove=(float)(zoom*0.8583+3.125);
		maxYmove=(float)(49.25-0.85*zoom);
		zoomCamera ();
		moveCamera ();
	}
	
	void zoomCamera()
	{
		if(Input.GetAxis("Mouse ScrollWheel")!=0)
		{
			zoom -= Input.GetAxis("Mouse ScrollWheel")*zoomSensitivity;
			zoom = Mathf.Clamp(zoom, zoommin, zoommax);
     		Camera.main.orthographicSize = zoom;
		}
	}
	
	void moveCamera()
	{
		if (Input.GetMouseButtonDown(0))
        {
            dragOrigin = Input.mousePosition;
            return;
        }
 
        if (!Input.GetMouseButton(0)) return;
 
        Vector3 pos = Camera.main.ScreenToViewportPoint(Input.mousePosition - dragOrigin);
        Vector3 move = new Vector3(-pos.x * dragSpeed,-pos.y * dragSpeed,0); 

		if(transform.position.x+move.x < minXmove)
		{
			move.x = minXmove-transform.position.x;
		}
		if(transform.position.x+move.x > maxXmove)
		{
			move.x = maxXmove-transform.position.x;
		}
		if(transform.position.y+move.y < minYmove)
		{
			move.y = minYmove-transform.position.y;
		}
		if(transform.position.y+move.y > maxYmove)
		{
			move.y = maxYmove-transform.position.y;
		}
		
        transform.Translate(move, Space.World);  
    }
}