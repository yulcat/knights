﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class InteractableObject : MonoBehaviour 
{
	public int MaxHP;
	public int HP;

	public Tile CurrentTile;

	public bool Buff;

	public List<Character> CharactersForAttack;

	public int totalDamage;

	public TextMesh DamageText;

	public void SetCurrentHP()
	{
		HP = MaxHP;
	}
}