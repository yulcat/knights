using UnityEngine;
using System.Collections;
using CommonConstants;

public class InformationText : MonoBehaviour 
{
	public TextMesh Text;

	public GameManager Manager;

	public bool Player;

	void Start()
	{
		if(GameManager.Characters.Count == Constants.ofTeamMembers*2)
		{
			Text.text = Manager.Names[GameManager.Characters[ReturnCPT_IfNotPlayer(Player)].CharacterIndex];
			
			for(int i = 1; i<Constants.ofTeamMembers; i++)
			{
				Text.text += "\n\n\n"+Manager.Names[GameManager.Characters[ReturnCPT_IfNotPlayer(Player)+i].CharacterIndex];
			}
		}
	}

	int ReturnCPT_IfNotPlayer(bool Player)
	{
		if(Player == true)
		{
			return 0;
		}
		else
		{
			return Constants.ofTeamMembers;
		}
	}
}