using UnityEngine;
using System.Collections;
using TeamsAndCharacters;
using CommonConstants;

public class UserSkill : MonoBehaviour
{
	public static AudioSource SE = null;

	public static AudioClip pintosUserSkillSound = null;
	public static AudioClip haskellUserSkillSound = null;
	public static AudioClip hitSound = null;

	void Awake()
	{
		SE = GetComponent<AudioSource>();
		pintosUserSkillSound = (AudioClip)Resources.Load("explosion");
		haskellUserSkillSound = (AudioClip)Resources.Load("twinkle2");
		hitSound = (AudioClip)Resources.Load("punch");
	}

/*	void ApplyPintosUserSkill(Character Knight, int SkillPower)
	{
		SE.PlayOneShot(pintosUserSkillSound, 1.0F);
		GameManager.ApplyDamage(Knight, 40*SkillPower);
		SE.PlayOneShot(hitSound, 2.0F);
	}

	void ApplyHaskellUserSkill(Character Knight, int SkillPower)
	{
		SE.PlayOneShot(haskellUserSkillSound, 1.0F);
		GameManager.ApplyHaskellBuff(Knight, SkillPower);
		SE.PlayOneShot(hitSound, 2.0F);
	}
*/
	IEnumerator OnMouseDown()
	{
		if(GameManager.Turn && GameManager.GlobalTurnPhase == GameManager.TurnPhases.Wait)
		{
			int SkillPower = 0;

			if(GameManager.myUserSkillCharge >= 100 && GameManager.myUserSkillCharge != 200)
			{
				SkillPower = 1;
			}
			else if(GameManager.myUserSkillCharge == 200)
			{
				SkillPower = 2;
			}

			if(SkillPower != 0)
			{
				foreach(Character Knight in GameManager.Characters)
				{
					if(GameManager.myTeam == Teams.Pintos && GameManager.isMyCharacter(Knight) == false)
					{
//						GameManager.ApplyDamage(Knight, 40*SkillPower);
						SE.PlayOneShot(pintosUserSkillSound, 1.0F);
						GameManager.ApplyDamage(Knight, 40*SkillPower);
						SE.PlayOneShot(hitSound, 1.0F);
						//ApplyPintosUserSkill(Knight, SkillPower);
					}
					else if(GameManager.myTeam == Teams.Haskell && GameManager.isMyCharacter(Knight) == true)
					{
//						GameManager.ApplyHaskellBuff(Knight, SkillPower);
						SE.PlayOneShot(haskellUserSkillSound, 1.0F);
						GameManager.ApplyHaskellBuff(Knight, SkillPower);
						//ApplyHaskellUserSkill(Knight, SkillPower);
					}
				}
			}

			GameManager.myUserSkillCharge -= 100*SkillPower;

			NetworkManager.SendUserSkillActivation();
		}

		yield return new WaitForEndOfFrame ();

		if(GameManager.aliveCharacterCount(true) == 0 || GameManager.aliveCharacterCount(false) == 0)
		{
			NoticeMessage Message = Instantiate (GameManager.StaticMessagePrefab, new Vector3 (Constants.MessageSpawnPositionX, 26.3f, -4), Quaternion.identity) as NoticeMessage;
			Message.GetComponent<SpriteRenderer> ().sprite = Message.GameEnd;
			Debug.Log ("All characters retired.");
		}
	}

	public static IEnumerator ActivateEnemyUserSkill()
	{
		int SkillPower = 0;

		if(GameManager.enemyUserSkillCharge >= 100 && GameManager.enemyUserSkillCharge != 200)
		{
			SkillPower = 1;
		}
		else if(GameManager.enemyUserSkillCharge == 200)
		{
			SkillPower = 2;
		}

		if(SkillPower != 0)
		{
			foreach(Character Knight in GameManager.Characters)
			{
				if(GameManager.otherTeam == Teams.Pintos && GameManager.isMyCharacter(Knight) == true)
				{
//					GameManager.ApplyDamage(Knight, 40*SkillPower);
					SE.PlayOneShot(pintosUserSkillSound, 1.0F);
					GameManager.ApplyDamage(Knight, 40*SkillPower);
					SE.PlayOneShot(hitSound, 1.0F);
					//ApplyPintosUserSkill(Knight, SkillPower);
				}
				else if(GameManager.otherTeam == Teams.Haskell && GameManager.isMyCharacter(Knight) == false)
				{
//					GameManager.ApplyHaskellBuff(Knight, SkillPower);
					SE.PlayOneShot(haskellUserSkillSound, 1.0F);
					GameManager.ApplyHaskellBuff(Knight, SkillPower);
					//ApplyHaskellUserSkill(Knight, SkillPower);
				}
			}
		}

		GameManager.enemyUserSkillCharge -= 100*SkillPower;

		yield return new WaitForEndOfFrame ();

		if(GameManager.aliveCharacterCount(true) == 0 || GameManager.aliveCharacterCount(false) == 0)
		{
			NoticeMessage Message = Instantiate (GameManager.StaticMessagePrefab, new Vector3 (Constants.MessageSpawnPositionX, 26.3f, -4), Quaternion.identity) as NoticeMessage;
			Message.GetComponent<SpriteRenderer> ().sprite = Message.GameEnd;
			Debug.Log ("All characters retired.");
		}
	}
}