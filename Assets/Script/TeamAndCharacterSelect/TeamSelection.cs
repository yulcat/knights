using UnityEngine;
using System.Collections;
using TeamsAndCharacters;

public class TeamSelection : MonoBehaviour 
{
	public Teams team;

	void OnMouseOver()
	{
		Activate();
	}
	
	void OnMouseExit ()
	{
		Deactivate();
	}
	
	void Start ()
	{
		Deactivate();
	}
	
	void OnMouseDown()
	{
		if(team == Teams.Pintos)
		{
			GameManager.myTeam = Teams.Pintos;
			NetworkManager.SendTeamInfo("Pintos");
		}
		else
		{
			GameManager.myTeam = Teams.Haskell;
			NetworkManager.SendTeamInfo("Haskell");
		}
		Application.LoadLevel ("CharacterSelect_M_inProcess");
	}
	
	void Deactivate ()
	{
		gameObject.renderer.material.color = Color.gray;
	}
	
	void Activate ()
	{
		gameObject.renderer.material.color = Color.white;
	}
	
}
