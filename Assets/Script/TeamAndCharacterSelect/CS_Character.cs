using UnityEngine;
using System.Collections;
using TeamsAndCharacters;

public class CS_Character : MonoBehaviour 
{	
	public Sprite[] pintos;
	public Sprite[] haskell;
	
	private SpriteRenderer sr;

	void Start () 
	{
		sr = GetComponent<SpriteRenderer>();
	}

	void Update () 
	{
		if (Selected.current < 10)
		{
			if (GameManager.myTeam == Teams.Haskell)
			{
				sr.sprite=haskell[Selected.current];
			}
			else if (GameManager.myTeam == Teams.Pintos)
			{
				sr.sprite=pintos[Selected.current];
			}
		}
	}
}
