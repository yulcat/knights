﻿using UnityEngine;
using System.Collections;
using TeamsAndCharacters;
using CommonConstants;

public class CS_ReadyButton : MonoBehaviour {

	public Sprite deactivatedReadyButton;
	public Sprite activatedReadyButton;

	private SpriteRenderer sr;

	// Use this for initialization
	void Start () 
	{
		sr = GetComponent<SpriteRenderer>();
		Deactivate();
	}
	
	// Update is called once per frame
	void Update()
	{
		if(CS_SelectedCharacters.numberOfSelectedCharacters==Constants.ofTeamMembers)
		{
			if(Network.isServer && NetworkManager.isClientReady)
			{
				Activate();
			}
			else if(Network.isClient)
			{
				Activate();
			}
		}
		else
		{
			Deactivate();	
		}
	
	}
	
	void OnMouseDown()
	{
		for(int i = 0; i<Constants.ofTeamMembers; i++)
		{
			GameManager.mySelectedCharacters[i] = Selected.character[i];
		}
		NetworkManager.SendSelectedCharacters (Selected.character);

		if (Network.isServer) 
		{
			NetworkManager.CallStartInGame();
		}
		else if(Network.isClient)
		{
			NetworkManager.SendClientReady();
		}

	}
	
	void Deactivate(){
		sr.sprite = deactivatedReadyButton;
		gameObject.collider.enabled=false;
	}
	
	void Activate(){
		sr.sprite = activatedReadyButton;
		gameObject.collider.enabled=true;
	}
}
