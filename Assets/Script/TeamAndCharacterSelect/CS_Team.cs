using UnityEngine;
using System.Collections;
using TeamsAndCharacters;

public class CS_Team : MonoBehaviour
{
	public Sprite pintos;
	public Sprite haskel;
	
	private SpriteRenderer sr;
	
	// Use this for initialization
	void Start () 
	{
		sr = GetComponent<SpriteRenderer>();
		
		if(GameManager.myTeam==Teams.Pintos)
		{
			sr.sprite=pintos;
		}
		else if(GameManager.myTeam==Teams.Haskell)
		{
			sr.sprite=haskel;
		}
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
