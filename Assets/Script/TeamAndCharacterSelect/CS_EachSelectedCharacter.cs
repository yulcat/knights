﻿using UnityEngine;
using System.Collections;
using TeamsAndCharacters;

public class CS_EachSelectedCharacter : MonoBehaviour {

	public int slot;
	public Sprite[] pintos;
	public Sprite[] haskell;
	public GameObject canceledCharacter;
	
	private SpriteRenderer sr;

	void Start () 
	{
		sr = GetComponent<SpriteRenderer>();
	}

	void Update () 
	{
		if (GameManager.myTeam == Teams.Haskell)
		{
			sr.sprite=haskell[Selected.character[slot]];
		}
		else if (GameManager.myTeam == Teams.Pintos)
		{
			sr.sprite=pintos[Selected.character[slot]];
		}
		//10 = empty
	}
	
	void OnMouseDown() 
	{
		canceledCharacter=GameObject.Find (Selected.character[slot].ToString());
		canceledCharacter.GetComponent<CharacterSelect>().activate=true;
		Debug.Log (canceledCharacter);
		CS_SelectedCharacters.clickedSelectedCharacter[slot]=true;
	}
}
