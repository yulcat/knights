﻿using UnityEngine;
using System.Collections;

public class StartButton : MonoBehaviour 
{
	public Sprite deactivatedButton;
	public Sprite activatedButton;

	private SpriteRenderer sr;

	// Use this for initialization
	void Start ()
	{
		sr = GetComponent<SpriteRenderer>();
		Deactivate();
	}

	// Update is called once per frame
	void Update()
	{
		if(NetworkManager.GetNetworkInstance().MyReady || !NetworkManager.GetNetworkInstance().OtherReady)
		{
			Deactivate();
		}

		else
		{
			Activate();
		}
	}

	void Deactivate()
	{
		sr.sprite = deactivatedButton;
	}

	void Activate()
	{
		sr.sprite = activatedButton;
	}

	void OnMouseDown()
	{
		if (!NetworkManager.GetNetworkInstance().MyReady && NetworkManager.GetNetworkInstance().OtherReady)
		{
			LobbyAndRoomManager.GetLobbyAndRoomManager ().ChangeStateToReady ();
		}
		else
		{
			LobbyAndRoomManager.GetLobbyAndRoomManager ().ChangeStateToWait ();
		}
	}
}
