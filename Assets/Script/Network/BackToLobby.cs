﻿using UnityEngine;
using System.Collections;

public class BackToLobby : MonoBehaviour {

	public Sprite deactivatedButton;
	public Sprite activatedButton;

	private SpriteRenderer sr;

	// Use this for initialization
	void Start ()
	{
		sr = GetComponent<SpriteRenderer>();
		Deactivate();
	}

	// Update is called once per frame
	void Update()
	{
		if(NetworkManager.GetNetworkInstance().MyReady)
		{
			Deactivate();
		}
		else
		{
			Activate();
		}
	}

	void Deactivate(){
		sr.sprite = deactivatedButton;
		gameObject.collider2D.enabled=false;
	}

	void Activate(){
		sr.sprite = activatedButton;
		gameObject.collider2D.enabled=true;
	}

	void OnMouseDown()
	{
		LobbyAndRoomManager.GetLobbyAndRoomManager().LeaveRoom();
		NetworkManager.GetNetworkInstance().DestroyAndMakeNewNetworkInstance();
	}
}
