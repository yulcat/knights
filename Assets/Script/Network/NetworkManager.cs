﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using TeamsAndCharacters;
using CommonConstants;

public class NetworkManager : MonoBehaviour 
{
	public static NetworkManager networkInstance = null;

	public bool MyReady;
	public bool OtherReady;
	public static bool isClientReady;

	public GameManager GM;

	public static NetworkManager GetNetworkInstance()
	{
		return networkInstance;
	}

	public void DestroyAndMakeNewNetworkInstance()
	{
		Destroy(this, 0);
		networkInstance = gameObject.AddComponent<NetworkManager>();
	}

	void Awake()
	{
		networkInstance = this;
		Debug.Log("Awake");
	}

	void Start()
	{
		DontDestroyOnLoad (this);
	}

	public static void SendRandomSeed(int seedInfo)
	{
		networkInstance.networkView.RPC ("ReceiveRandomSeed", RPCMode.Others, seedInfo);
	}

	[RPC]
	void ReceiveRandomSeed(int seedInfo)
	{
		PlayerPrefs.SetInt ("randomSeed", seedInfo);
	}

	public static void SendReady()
	{
		networkInstance.networkView.RPC("ReceiveOtherReady", RPCMode.Others);
	}

	public static void SendWait()
	{
		networkInstance.networkView.RPC("ReceiveOtherWait", RPCMode.Others);
	}

	[RPC]
	void ReceiveOtherReady()
	{
		OtherReady = true;
	}

	[RPC]
	void ReceiveOtherWait()
	{
		OtherReady = false;
	}

	public static void SendTeamInfo(string TeamInfo)
	{
		networkInstance.networkView.RPC ("ReceiveTeamInfo", RPCMode.Others, TeamInfo);
	}

	[RPC]
	void ReceiveTeamInfo(string TeamInfo)
	{
		GameManager.SetOtherTeamInfo (TeamInfo);
	}

	public static void SendSelectedCharacters(int[] SelectedCharacters)
	{
		networkInstance.networkView.RPC ("ReceiveSelectedCharacters", RPCMode.Others, SelectedCharacters);
	}

	[RPC]
	void ReceiveSelectedCharacters(int[] SelectedCharacters)
	{
		for(int i = 0; i<Constants.ofTeamMembers; i++)
		{
			GameManager.otherSelectedCharacters[i] = SelectedCharacters[i];
		}
	}

	public static void SendClientReady()
	{
		networkInstance.networkView.RPC ("ReceiveClientReady", RPCMode.Others);
	}
	[RPC]
	void ReceiveClientReady()
	{
		isClientReady = true;
	}

	public static void CallStartInGame()
	{
		networkInstance.networkView.RPC ("StartInGame", RPCMode.All);
	}

	[RPC]
	void StartInGame()
	{
		CS_WaitingImage.CallWaitingImage ();
		Application.LoadLevel ("InGame");
	}

	public static void SendMoveInformation(int[] MoveInfo)
	{
		Debug.Log ("Send Move Information.");
		networkInstance.networkView.RPC ("ReceiveMoveInformation", RPCMode.Others, MoveInfo);
	}

	[RPC]
	void ReceiveMoveInformation(int[] MoveInfo)
	{
		GM.MoveAndFix_Character_BeforeMoveAni (MoveInfo[0], MoveInfo[1], MoveInfo[2], MoveInfo[3]);
	}

	public static void CallNextTurn()
	{
		networkInstance.networkView.RPC ("ToNextTurn", RPCMode.All);
	}

	[RPC]
	void ToNextTurn()
	{
		GM.ApplyAttacks ();
	}

	public static void SendUserSkillActivation()
	{
		networkInstance.networkView.RPC ("ReceiveUserSkill", RPCMode.Others);
	}

	[RPC]
	void ReceiveUserSkill()
	{
		StartCoroutine(UserSkill.ActivateEnemyUserSkill ());
	}

	void OnLevelWasLoaded(int Level)
	{
		if(Application.loadedLevelName == "CharacterSelect_M_inProcess" && Network.isServer)
		{
			int randomSeed = UnityEngine.Random.Range(1, 100);
			PlayerPrefs.SetInt("randomSeed", randomSeed);
			SendRandomSeed(randomSeed);
		}
		if(Application.loadedLevelName == "InGame")
		{
			GM = GameObject.FindObjectOfType (typeof(GameManager)) as GameManager;
		}
	}
}