﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Connect_text : MonoBehaviour {

	// Use this for initialization
	void Start () {
		GetComponent<MeshRenderer>().enabled = false;
	}

	void OnMouseDown()
	{
		GetComponent<MeshRenderer>().enabled = true;
	}

	// Update is called once per frame
	void Update () {
		bool isConnected = Network.connections.Length > 0;

		GetComponent<TextMesh>().text = "Connect : " + isConnected.ToString();
	}
}
