﻿using UnityEngine;
using System.Collections;

public class BackToTitle : MonoBehaviour {

	void OnMouseDown()
	{
		LobbyAndRoomManager.GetLobbyAndRoomManager().LeaveLobby();
	}
}
