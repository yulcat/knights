using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class NetworkInfo : MonoBehaviour {

	public static string myIp = "";
	public static readonly int ServerPortNum = 25252;
	public static int roomNumForDebug = 0;

	public static string othersIp = "---.---.---.---";

	void Awake()
	{
		myIp = Network.player.ipAddress;
	}

	public static bool IsEnteredRoom()
	{
		return Network.connections.Length != 0;
	}
}
