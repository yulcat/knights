﻿using UnityEngine;
using System.Collections;

public class LobbyAndRoomManager : MonoBehaviour {

	public static LobbyAndRoomManager lobbyAndRoomInstance = null;

	public GameObject camera;

	GameObject startButton;
	GameObject readyButton;

	public static LobbyAndRoomManager GetLobbyAndRoomManager()
	{
		return lobbyAndRoomInstance;
	}

	public void MoveCameraToRoom()
	{
		camera.transform.position += Vector3.right * 20;
	}

	public void MoveCameraToLobby()
	{
		camera.transform.position += Vector3.left * 20;
	}

	public void DestroyGameObject(string tagOfGameObject)
	{
		GameObject deleteTargetObject = GameObject.FindWithTag(tagOfGameObject);
		if (deleteTargetObject != null)
		{
			Destroy(deleteTargetObject);
		}
	}

	static void StartWaitingClient ()
	{
		Network.InitializeServer (connections: 10, listenPort: NetworkInfo.ServerPortNum, useNat: false);
		NetworkInfo.roomNumForDebug = 23;
	}

	public void CreateRoom()
	{
		if (!NetworkInfo.IsEnteredRoom())
		{
			StartWaitingClient ();
			NoticeText.noticeText = "Creating new room.";

			readyButton.SetActive(false);

			MoveCameraToRoom ();
		}
		else
		{
			NoticeText.noticeText = "Already entered room.";
		}
	}

	//Using LocalTest.
	public void JoinLocalRoom()
	{
		if (!NetworkInfo.IsEnteredRoom ())
		{
			Network.Connect("localhost", NetworkInfo.ServerPortNum);
			NetworkInfo.roomNumForDebug = 23;

			NoticeText.noticeText = "Entering room. (only using localTest)";

			startButton.SetActive(false);

			MoveCameraToRoom ();
		}
		else
		{
			NoticeText.noticeText = "Already entered room.";
		}
	}

	//Input ip of Host before run.
	public void JoinRoom()
	{
		if (!NetworkInfo.IsEnteredRoom ())
		{
			Network.Connect (NetworkInfo.othersIp, NetworkInfo.ServerPortNum);
			NetworkInfo.roomNumForDebug = 23;

			NoticeText.noticeText = "Entering room.";

			startButton.SetActive(false);

			MoveCameraToRoom ();
		}
		else
		{
			NoticeText.noticeText = "Already entered room.";
		}
	}

	public void LeaveRoom()
	{
		if (true)
		{
			NetworkManager.GetNetworkInstance().MyReady = false;
			NetworkManager.SendReady();

			Network.Disconnect(0);

			startButton.SetActive(true);
			readyButton.SetActive(true);

			NoticeText.noticeText = "Back to lobby.";

			MoveCameraToLobby();
		}
	}

	public void LeaveRoomWhenHostIsGone()
	{
		if (Network.connections.Length == 1 && Network.isClient)
		{
			LeaveRoom();
			NoticeText.noticeText = "Room is destroyed.";
		}
	}

	public void LeaveLobby()
	{
		DestroyGameObject("NetworkManager");
		Application.LoadLevel("TitleScene");
	}

	public void ChangeStateToReady()
	{
		if (!NetworkManager.GetNetworkInstance().MyReady && NetworkInfo.IsEnteredRoom())
		{
			NetworkManager.GetNetworkInstance().MyReady = true;
			NetworkManager.SendReady();
		}
		else if (!NetworkInfo.IsEnteredRoom())
		{
			NoticeText.noticeText = "Please wait other player.";
		}
		else
		{
			NoticeText.noticeText = "Already readied.";
		}
	}

	public void ChangeStateToWait()
	{
		NetworkManager.GetNetworkInstance().MyReady = false;
		NetworkManager.SendWait();
	}

	void Awake()
	{
		lobbyAndRoomInstance = this;
	}

	// Use this for initialization
	void Start () {
		startButton = GameObject.Find("StartButton(Host)");
		readyButton = GameObject.Find("ReadyButton(Guest)");
	}

	// Update is called once per frame
	void Update () {
		//LeaveRoomWhenHostIsGone();
	}
}
