﻿using UnityEngine;
using System.Collections;
using CommonConstants;

public class LocalInformationText : MonoBehaviour
{
	public TextMesh Text;
	
	public LocalManager Manager;
	
	public bool isPlayer1P;

//	private string test="";

	void Start()
	{
//		test = Manager.Characters [0].name;
		if(Manager.Characters.Count == Constants.ofTeamMembers*2)
		{
			Text.text = Manager.Names[Manager.Characters[ReturnCharacterCount_IfPlayer2(isPlayer1P)].CharacterIndex];
			
			for(int i = 1; i<Constants.ofTeamMembers; i++)
			{
				Text.text += "\n\n\n"+Manager.Names[Manager.Characters[ReturnCharacterCount_IfPlayer2(isPlayer1P)+i].CharacterIndex];
			}
		}
	}

	int ReturnCharacterCount_IfPlayer2(bool player)
	{
		if (player == true)
			return 0;
		else
			return Constants.ofTeamMembers;
	}
}
