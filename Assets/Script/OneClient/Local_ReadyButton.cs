﻿using UnityEngine;
using System.Collections;
using CommonConstants;

public class Local_ReadyButton : MonoBehaviour
{
	public Local_SelectionData selection;
	public Local_SelectedCharacters selected;
	public Sprite deactivatedReadyButton;
	public Sprite activatedReadyButton;
	private SpriteRenderer sr;

	void FinishSelect()
	{
		if(selection.currentPlayer == 1)
		{
			selection.currentPlayer++;
			Application.LoadLevel("LocalTeamSelect2");
		}
		else
		{
			CS_WaitingImage.CallWaitingImage();
			Application.LoadLevel("LocalInGame");
		}
	}

	void Start ()
	{
		selection = GameObject.FindObjectOfType (typeof(Local_SelectionData)) as Local_SelectionData;
		selected = GameObject.FindObjectOfType (typeof(Local_SelectedCharacters)) as Local_SelectedCharacters;
		sr = GetComponent<SpriteRenderer> ();
		Deactivate();
	}

	void Update()
	{
		if (Local_ReadyCounter.readyCount == Constants.ofTeamMembers)
			Activate ();
		else
			Deactivate ();
	}
	
	void OnMouseDown()
	{
		if(Local_ReadyCounter.readyCount==Constants.ofTeamMembers)
		{
			FinishSelect();
		}
	}

	void Deactivate()
	{
		sr.sprite = deactivatedReadyButton;
		gameObject.collider.enabled=false;
	}
	
	void Activate()
	{
		sr.sprite = activatedReadyButton;
		gameObject.collider.enabled=true;
	}
}
