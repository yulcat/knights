﻿using UnityEngine;
using System.Collections;
using TeamsAndCharacters;
using CommonConstants;

public class LocalCharacterSelect : MonoBehaviour {
	public Local_SelectionData selection;
	public bool activate;
	public PintosCharacter pintosCharacterForThisSlot;
	public HaskellCharacter haskellCharacterForThisSlot;

	void Start()
	{
		selection = GameObject.FindObjectOfType(typeof(Local_SelectionData)) as Local_SelectionData;
	}
	
	void OnMouseDown()
	{
		if(Local_ReadyCounter.readyCount< Constants.ofTeamMembers)
		{
			Local_ReadyCounter.clicked = true;
			Deactivate();
		}
	}

	void OnMouseOver()
	{
		if(GameManager.myTeam == Teams.Haskell)
		{
			selection.currentSelection = (int)haskellCharacterForThisSlot;
		}
		else if(GameManager.myTeam == Teams.Pintos)
		{
			selection.currentSelection = (int)pintosCharacterForThisSlot;
		}
	}
	void OnMouseExit () 
	{
		selection.currentSelection=Constants.ofCharactersInEachTeam; 
	}
	
	void Update ()
	{
		if(activate)
		{
			Activate ();
			activate=false;
		}
	}
	
	void Deactivate()
	{
		gameObject.renderer.material.color = Color.gray;
		gameObject.collider.enabled=false;
	}
	
	void Activate () 
	{
		gameObject.renderer.material.color = Color.white;
		gameObject.collider.enabled=true;
	}
}
