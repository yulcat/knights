﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using TeamsAndCharacters;
using CommonConstants;

public class LocalTile : MonoBehaviour
{
	public LocalManager manager;
	
	public int X;
	public int Y;
	
	public LocalCharacter CharacterOnTile;
	public LocalBuff BuffOnTile;
	
	public List<LocalCharacter> CharactersForAttack;
	
	public SpriteRenderer MyRenderer;
	
	public Sprite DefaultSprite;
	public Sprite MovableSprite;
	public Sprite AttackableSprite;
	public Sprite MovableAndAttackableSprite;
	
	public bool Movable;
	
	public bool Occupied;

	public Local_Check localChecker;

	public void OnMouseDown()
	{
		if(Movable == true)
		{
			manager.MovingCharacter.StartTile = manager.MovingCharacter.CurrentTile;
			manager.MovingCharacter.startDirection = manager.MovingCharacter.Direction;
			manager.MovingCharacter.startScaleX = manager.MovingCharacter.transform.localScale.x;
			manager.MovingCharacter.CurrentTile.CharacterOnTile = null;
			manager.MovingCharacter.CurrentTile = this;
			
			iTween.MoveTo(manager.MovingCharacter.gameObject, iTween.Hash ("position", transform.position + new Vector3(0, 1, -1), "Speed", Constants.MoveAniSpeed, "easeType", "linear", "oncompletetarget", this.gameObject, "oncomplete", "AfterMoveAni"));
			manager.MovingCharacter.GetComponent<Animator>().SetBool("Walk", true);
			
			CharacterOnTile = manager.MovingCharacter;
			
			manager.DeactivateAllTiles();

			manager.ActivateLocalCharacterColliders();
		}
	}
	
	public void AfterMoveAni()
	{
		manager.MovingCharacter.TurnPhase = LocalManager.TurnPhases.Direction;
		manager.GlobalTurnPhase = LocalManager.TurnPhases.Direction;
		List<LocalTile> TileList = manager.MovingCharacter.ShowAndReturn_AttackRange();
		
		manager.MovingCharacter.GetComponent<Animator>().SetBool("Walk", false);
	}	
}
