﻿using UnityEngine;
using System.Collections;
using TeamsAndCharacters;
using CommonConstants;

public class LocalCutScene : MonoBehaviour
{
	public int chainCount;
	
	public Teams Team;
	
	public Material[] Dividers;
	public Texture[] CharacterTextures;
	
	public Renderer Divider;
	public Renderer[] CharacterRenderers;
	
	public LocalInteractableObject MostChainedObject;
	
	//컷씬이 화면 중앙에서 멈춰있는 시간(?)
	public float PauseTime = 0;
	
	//사운드이펙트
	public AudioClip AppearSound;
	//public AudioClip DisappearSound;
	public AudioSource SoundEffect;
	
	void Awake()
	{
		SoundEffect = GetComponent<AudioSource>();
	}
	
	void Start()
	{
		Divider.material = Dividers [chainCount - 2 + return3ifHaskell ()];
		
		for(int i = 0; i < chainCount; i++)
		{
			if(CharacterTextures[MostChainedObject.CharactersForAttack [i].CharacterIndex] != null)
			{
				CharacterRenderers [i].material.mainTexture = CharacterTextures[MostChainedObject.CharactersForAttack [i].CharacterIndex];
			}
		}
		
		SoundEffect.PlayOneShot(AppearSound, 2.0f);
		
		iTween.MoveTo(gameObject, iTween.Hash ("position", new Vector3(26.3f, 26.3f, 0), "Speed", Constants.CutSceneSpeed, "easeType", "linear", "oncompletetarget", gameObject, "oncomplete", "Restart"));
	}
	
	IEnumerator Restart()
	{
		yield return new WaitForSeconds (PauseTime);
		
		//SoundEffect.PlayOneShot(DisappearSound, 1.0f);
		
		iTween.MoveTo(gameObject, iTween.Hash ("position", new Vector3(26.3f, 96.3f, 0), "Speed", Constants.CutSceneSpeed, "easeType", "linear", "oncompletetarget", gameObject, "oncomplete", "DestroyMyself"));
	}
	
	void DestroyMyself()
	{
		LocalManager Manager = LocalManager.FindObjectOfType (typeof(LocalManager)) as LocalManager;
		StartCoroutine(GameManager.Attackers[0].DoAttack(0));
	}
	
	int return3ifHaskell()
	{
		if(Team == Teams.Pintos)
		{
			return 0;
		}
		else
		{
			return 3;
		}
	}
}
