using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using TeamsAndCharacters;

public class Local_SelectionData : MonoBehaviour {
	public Teams team1;
	public Teams team2;
	public int currentPlayer;
	public Teams currentTeam;
	public int currentSelection;
	public int[] character1;
	public int[] character2;

	void Start()
	{
		DontDestroyOnLoad (gameObject);
		currentPlayer = 1;
	}

	public int[] characters(int player)
	{
		if (player == 1)
			return character1;
		else
			return character2;
	}

	public Teams getCurrentTeam (int player)
	{
		if (player == 1)
			return team1;
		else
			return team2;
	}
}
