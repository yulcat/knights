﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class LocalInteractableObject : MonoBehaviour 
{
	public int MaxHP;
	public int HP;
	
	public LocalTile CurrentTile;
	
	public bool Buff;
	
	public List<LocalCharacter> CharactersForAttack;
	
	public int totalDamage;
	
	public TextMesh DamageText;
	
	public void SetCurrentHP()
	{
		HP = MaxHP;
	}
}