using UnityEngine;
using System.Collections;
using TeamsAndCharacters;
using CommonConstants;

public class Local_EachSelectedCharacters : MonoBehaviour
{
	
	public Sprite[] pintos;
	public Sprite[] haskell;
	public int slot;
	public Local_SelectionData selection;
	public Local_ReadyCounter counter;
	public GameObject[] characters;
	public GameObject canceledCharacter;
	
	private SpriteRenderer sr;

	void Start ()
	{
		selection = GameObject.FindObjectOfType (typeof(Local_SelectionData)) as Local_SelectionData;
		sr = GetComponent<SpriteRenderer>();
		sr.sprite = pintos [Constants.ofCharactersInEachTeam];
	}

	void Update () 
	{
		if (selection.getCurrentTeam (selection.currentPlayer) == Teams.Haskell) {
//			if(slot == Local_ReadyCounter.readyCount)
				sr.sprite = haskell [selection.characters (selection.currentPlayer) [slot]];
		} else if (selection.getCurrentTeam (selection.currentPlayer) == Teams.Pintos) {
//			if(slot == Local_ReadyCounter.readyCount)
				sr.sprite = pintos [selection.characters (selection.currentPlayer) [slot]];
		}
		//10 = empty
	}
	
	void OnMouseDown() 
	{
		canceledCharacter = characters[selection.currentSelection];
		canceledCharacter.GetComponent<LocalCharacterSelect>().activate=true;
		Local_SelectedCharacters.clickedSelectedCharacter[slot]=true;
	}
}