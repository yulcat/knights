﻿using UnityEngine;
using System.Collections;

public class LocalCharacterPart : MonoBehaviour
{
	public Sprite[] Sprites;
	
	public LocalCharacter ParentCharacter;
	
	void Start()
	{
		GetComponent<SpriteRenderer> ().sprite = Sprites [ParentCharacter.CharacterIndex];
	}
}
