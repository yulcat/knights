﻿using UnityEngine;
using System.Collections;
using TeamsAndCharacters;
using CommonConstants;

public class Local_SelectedCharacters : MonoBehaviour
{
	public static bool[] clickedSelectedCharacter = new bool[Constants.ofTeamMembers];
	public static int[] slot = new int[Constants.ofTeamMembers];
	public Local_SelectionData selection;
	
	void Start () 
	{
		selection = GameObject.FindObjectOfType (typeof(Local_SelectionData)) as Local_SelectionData;
		for(int i = 0; i < Constants.ofTeamMembers ; i ++)
		{
			slot[i] = Constants.ofCharactersInEachTeam; //anybignumber
			clickedSelectedCharacter[i]=false;
		}
	}

	void Update () 
	{
		if(Local_ReadyCounter.clicked)
		{
			for(int i = 0; i < Constants.ofTeamMembers ; i ++)
			{
				if(slot[i]==Constants.ofCharactersInEachTeam)
				{
					slot[i]=selection.currentSelection;
					selection.characters(selection.currentPlayer)[i]=slot[i];
					Local_ReadyCounter.clicked=false;
					Local_ReadyCounter.readyCount++;
					break;
				}
			}
		}
		
		for(int i = 0; i < Constants.ofTeamMembers ; i ++){
			if(clickedSelectedCharacter[i]){
				selection.characters(selection.currentPlayer)[i]=Constants.ofCharactersInEachTeam;
				slot[i]=Constants.ofCharactersInEachTeam;
				Local_ReadyCounter.readyCount--;
				clickedSelectedCharacter[i]=false;
				break;
			}
		}
	}
}
